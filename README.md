# This repo is for the IRODS project from IFREMER. 

The goal is to provide a way for a scientist, from a jupyter environment, to create and get a subset of the data available to work with. 

## Set up 

add a custom rule in the IRODS cluster named "subset", add the subset.re file.
add the subset.sh script in the msi_exec folder in /var/lib/irods.

Build the docker image using the Dockerfile. 

## How to use

When adding a yaml file, the custom rule will trigger the script. 
The script will pass the yaml intake file to a docker container to create the subset.

The subset will then be zip and push to irods, to be get on the VRE using the function in the python files.