# coding: utf-8

import dask
import intake
import numpy as np
import xarray as xr
import time

ts = time.time()
cat = intake.open_catalog("/workdir/intakes/intake_catalog.yaml")
zarr_output = "/workdir/subsets/results"

list(cat)

print(cat.sea_surface_height.metadata["start_date_requested"])
print(cat.sea_surface_height.metadata["end_date_requested"])
print(cat.sea_surface_height.metadata["variables_requested"])
print(cat.sea_surface_height.metadata["chunk_requested"])


with dask.config.set(**{'array.slicing.split_large_chunks': False}):
    
    ds = cat.sea_surface_height().to_dask()
    
    subsets = []
    for v in cat.sea_surface_height.metadata["variables_requested"]:
        subset = ds[v][(ds.time >= np.datetime64(cat.sea_surface_height.metadata["start_date_requested"])) &
                       (ds.time <= np.datetime64(cat.sea_surface_height.metadata["end_date_requested"]))]
        subsets.append(subset)

    
    
    subset = xr.merge(subsets, join="exact")
    subset = subset.chunk(cat.sea_surface_height.metadata["chunk_requested"])

    subset
    
    subset.chunks

    # subset.to_zarr(zarr_output, encoding={"chunks": (60, 720, 1440)})
    subset.to_zarr(zarr_output)