pep_api_data_obj_put_post(*INSTANCE_NAME, *COMM, *DATAOBJINP, *BUFFER, *PORTAL_OPR_OUT) {
  *obj_path = *DATAOBJINP.obj_path ;
  *user = *COMM.user_user_name ;
  writeLine("serverLog" , "*user stored object *obj_path");
  *cmd = "subset.sh" ;
  msiSetACL( "default" , "read" , "rods" , *obj_path );
  msiExecCmd( *cmd , *obj_path , "null" , "null" , "null" , *result );
  msiGetStdoutInExecCmdOut( *result , *Out );
  writeLine("serverLog" , "Output of *cmd is: *Out");
}  
