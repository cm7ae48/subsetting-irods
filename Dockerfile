FROM python:3
MAINTAINER charly.maupetit@ifremer.fr
RUN pip install --no-cache-dir zarr==2.4.0 xarray==0.16.0 matplotlib numpy netcdf4 dask==2.30.0 intake==0.6.0 intake-xarray intake-parquet
RUN mkdir /src/
RUN mkdir -p /workdir/subsets
WORKDIR /src/
COPY generate_subset.py /src/
ENTRYPOINT ["python", "/src/generate_subset.py"]

# to run
# docker run -v /export/home/zarr/zarr.90_days:/workdir/zarr -v /tmp/intakes:/workdir/intakes -v /export/home/subsets/:/workdir/subsets -it subset_zarr

# /home/datawork-data-terra/eosc-pillar/notebooks/data_subset parquet_script