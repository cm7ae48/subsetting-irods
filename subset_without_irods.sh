#!/bin/bash

# file to put in /var/lib/irods/msiExecCmd_bin
# that will be trigger by subset.sh custom rule in irods
# $1 absolute path from file in irods

# echo $1 > /tmp/toto

if [[ $1 == *.yaml ]]
then
    # filename = username on VRE + timestamp when creating the yaml file on VRE
    filename=$(basename -- "$1")
    extension="${filename##*.}"
    filename="${filename%.*}"
    timestamp=$(date +%s)
    output_directory=/export/home/subsets/run_${filename}
    # create a temp folder 
    cd /tmp/
    mkdir -p ${filename}
    # get the yaml intake file into the temp folder
    # /bin/iget $1 /tmp/${filename}
    # rename the yaml file to match the script expectation
    #CHANGE THIS when used with irods, this is just for test
    cp $1 /tmp/${filename}/intake_catalog.yaml
    # make a directory that will contain the subset, with matching name of the yaml file, so we can get it in the python script 
    mkdir -p ${output_directory}
    chmod 777 -R ${output_directory}
    # run the subset script in a docker container 
    # we add a variable to the container name for the loop after
    # we mount the 90days dataset on the containter
    # we mount the directory with the intake file on /workdir/intakes
    # finally, we mount the folder to get the subset
    /bin/docker run --name subset${timestamp} -v /export/home/zarr/zarr.90_days:/workdir/zarr -v /tmp/${filename}/:/workdir/intakes -v ${output_directory}:/workdir/subsets subset_zarr >/tmp/out 2>&1

    while [ "`docker inspect -f {{.State.Running}} subset${timestamp}`" != "false" ]; do     sleep 2; done

    cd /export/home/subsets
    cp /tmp/out run_${filename}
    # we now compress the subset so it will be easier to move it around networks
    tar -zcvf run_${filename}.tar.gz run_${filename}
    # adding that to irods
    # /bin/iput -r run_${filename}.tar.gz /testifremer/subsets
    # cleaning
    rm -f run${filename}
fi
