import os

# slow, do not use except for test
def iget(session, irods_path, local_path, recursive=False):
    """
        Download files from an iRODS server.
        Args:
            session (iRODS.session.iRODSSession): iRODS session
            irods_path (String): File or folder path to get
                from the iRODS server. Must be absolute path.
            local_path (String): local folder to place the downloaded files in
            recursive (Boolean): recursively get folders.
    """
    if session.data_objects.exists(irods_path):
        to_file_path = os.path.join(local_path, os.path.basename(irods_path))
        session.data_objects.get(irods_path, file=to_file_path)
    elif session.collections.exists(irods_path):
        if recursive:
            coll = session.collections.get(irods_path)
            local_path = os.path.join(local_path, os.path.basename(irods_path))
            os.mkdir(local_path)

            print(coll.data_objects)

            for file_object in coll.data_objects:
                print(
                    f"Retrieving data object : {file_object} to dir ==> {local_path}")
                session.data_objects.get(os.path.join(
                    irods_path, file_object.path), local_path=local_path)
#                 get(session, os.path.join(irods_path, file_object.path), local_path, True)
            for collection in coll.subcollections:
                iget(session, collection.path, local_path, True)
        else:
            raise FileNotFoundError("Skipping directory " + irods_path)
    else:
        raise FileNotFoundError(irods_path + " Does not exist")
