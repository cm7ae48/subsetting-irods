import os
import ssl
import posixpath
from irods.session import iRODSSession
import yaml 
import getpass
import time
import tarfile

# Five steps
# 1st: generate a yaml file with given parameters
# 2nd: put yaml file on irods
# 3rd: trigger the subset script and generate a tarball username_timestamp.tar.gz
# 4th: add metadata
# 5th: recover the tarball on vre and unpack to be ready to use

## TODO: when creating the user, create irods account
## TODO: add chksum to archive metadata to check when pulling is done

class IfremerIrods:

    irods_icat_url = '134.246.134.40'  # virods-icat ifremer
    irods_data = '/jupyter_data'
    irods_intake_template = irods_data + "/intake_template.yaml"
    irods_directory = '/Ifremer/irods_data'

    def __init__(self):
        self.timestamp = str(int(time.time()))
        self.irods_session = self.irods_session()
        self.file_name = getpass.getuser() + "_" + self.timestamp

    # generate the yaml file
    def generate_subset(self,start_date, end_date, chunk_size=90):
        with open(self.irods_intake_template) as f:
            doc = yaml.load(f, Loader=yaml.FullLoader)

        doc['sources']['sea_surface_height']['metadata']['start_date_requested'] = start_date
        doc['sources']['sea_surface_height']['metadata']['end_date_requested'] = end_date
        doc['sources']['sea_surface_height']['metadata']['chunk_requested'] = chunk_size

        with open(self.irods_data + self.get_yaml_name() + '.yaml', 'w+') as f:
            yaml.dump(doc, f)


    def recover_subset(self): 
        # TODO: polling system here
        self.irods_session.data_objects.get(self.file_name, self.irods_directory)
        while(not os.path.isfile(self.irods_directory+'/'+self.file_name+'.tar.gz')):
            print("recovering data...")
            time.sleep(2)
        tar = tarfile.open(self.irods_directory+'/' +
                           self.file_name+'.tar.gz', "r:gz")
        tar.extractall(path=self.irods_data)
        tar.close()
        



    def irods_session(self):
        return iRODSSession(host=self.irods_icat_url, port=1247, user='rods', password='rods', zone='Ifremer')

    # put the yaml file on irods to generate subset
    def put_subset(self,yaml_file):
        self.irods_session.data_objects.put(yaml_file, self.irods_directory)

    def get_yaml_name(self):
        return self.irods_data + "/" + self.file_name + ".yaml"
